# 至简魔方首页主题

#### 介绍

至简魔方的首页主题模板

#### 站点展示
[展示站点](https://idc.yuyungu.com)
![图片展示1](https://foruda.gitee.com/images/1711854301019375200/abcf8950_12350512.png "屏幕截图 2024-03-31 110408.png")
![图片展示2](https://foruda.gitee.com/images/1711854318934168217/f1c7e59f_12350512.png "屏幕截图 2024-03-31 110423.png")
![图片展示3](https://foruda.gitee.com/images/1711854340845470569/38736233_12350512.png "屏幕截图 2024-03-31 110436.png")
![图片展示4](https://foruda.gitee.com/images/1711854360634467016/d80a2401_12350512.png "屏幕截图 2024-03-31 110453.png")

#### 安装教程

1.  上传源码至至简魔方/public/themes/web
2.  修改至简魔方后台配置即可


